package com.gnewitt.rest.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "driverLicenseRequired", "requiredCertificates","location","billRate","workersRequired","startDate","about","jobTitle","company","guid","jobId" })
public class Job
{
	Boolean driverLicenseRequired;
	String requiredCertificates[];
	Location location;
	String billRate;
	Integer workersRequired;
	Date startDate;
	String about;
	String jobTitle;
	String company;
	String guid;
	Integer jobId;
	/**
	 * @return the driverLicenseRequired
	 */
	public Boolean getDriverLicenseRequired()
	{
		return driverLicenseRequired;
	}
	/**
	 * @param driverLicenseRequired the driverLicenseRequired to set
	 */
	public void setDriverLicenseRequired(Boolean driverLicenseRequired)
	{
		this.driverLicenseRequired = driverLicenseRequired;
	}
	/**
	 * @return the requiredCertificates
	 */
	public String[] getRequiredCertificates()
	{
		return requiredCertificates;
	}
	/**
	 * @param requiredCertificates the requiredCertificates to set
	 */
	public void setRequiredCertificates(String[] requiredCertificates)
	{
		this.requiredCertificates = requiredCertificates;
	}
	/**
	 * @return the location
	 */
	public Location getLocation()
	{
		return location;
	}
	/**
	 * @param location the location to set
	 */
	public void setLocation(Location location)
	{
		this.location = location;
	}
	/**
	 * @return the billRate
	 */
	public String getBillRate()
	{
		return billRate;
	}
	/**
	 * @param billRate the billRate to set
	 */
	public void setBillRate(String billRate)
	{
		this.billRate = billRate;
	}
	/**
	 * @return the workersRequired
	 */
	public Integer getWorkersRequired()
	{
		return workersRequired;
	}
	/**
	 * @param workersRequired the workersRequired to set
	 */
	public void setWorkersRequired(Integer workersRequired)
	{
		this.workersRequired = workersRequired;
	}
	/**
	 * @return the startDate
	 */
	public Date getStartDate()
	{
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}
	/**
	 * @return the about
	 */
	public String getAbout()
	{
		return about;
	}
	/**
	 * @param about the about to set
	 */
	public void setAbout(String about)
	{
		this.about = about;
	}
	/**
	 * @return the jobTitle
	 */
	public String getJobTitle()
	{
		return jobTitle;
	}
	/**
	 * @param jobTitle the jobTitle to set
	 */
	public void setJobTitle(String jobTitle)
	{
		this.jobTitle = jobTitle;
	}
	/**
	 * @return the company
	 */
	public String getCompany()
	{
		return company;
	}
	/**
	 * @param company the company to set
	 */
	public void setCompany(String company)
	{
		this.company = company;
	}
	/**
	 * @return the guid
	 */
	public String getGuid()
	{
		return guid;
	}
	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid)
	{
		this.guid = guid;
	}
	/**
	 * @return the jobId
	 */
	public Integer getJobId()
	{
		return jobId;
	}
	/**
	 * @param jobId the jobId to set
	 */
	public void setJobId(Integer jobId)
	{
		this.jobId = jobId;
	}
}
