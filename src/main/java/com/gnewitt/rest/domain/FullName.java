package com.gnewitt.rest.domain;

public class FullName
{
	String last;
	String first;
	/**
	 * @return the last
	 */
	public String getLast()
	{
		return last;
	}
	/**
	 * @param last the last to set
	 */
	public void setLast(String last)
	{
		this.last = last;
	}
	/**
	 * @return the first
	 */
	public String getFirst()
	{
		return first;
	}
	/**
	 * @param first the first to set
	 */
	public void setFirst(String first)
	{
		this.first = first;
	}
}
