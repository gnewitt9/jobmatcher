package com.gnewitt.rest.domain;

public class Worker
{
	Integer rating;
	Boolean isActive;
	String certificates[];
	String skills[];
	JobSearchAddress jobSearchAddress;
	String transportation;
	Boolean hasDriversLicense;
	Day availability[];
	String phone;
	String email;
	FullName name;
	Integer age;
	String guid;
	Integer userId;
	/**
	 * @return the rating
	 */
	public Integer getRating()
	{
		return rating;
	}
	/**
	 * @param rating the rating to set
	 */
	public void setRating(Integer rating)
	{
		this.rating = rating;
	}
	/**
	 * @return the isActive
	 */
	public Boolean getIsActive()
	{
		return isActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(Boolean isActive)
	{
		this.isActive = isActive;
	}
	/**
	 * @return the certificates
	 */
	public String[] getCertificates()
	{
		return certificates;
	}
	/**
	 * @param certificates the certificates to set
	 */
	public void setCertificates(String[] certificates)
	{
		this.certificates = certificates;
	}
	/**
	 * @return the skills
	 */
	public String[] getSkills()
	{
		return skills;
	}
	/**
	 * @param skills the skills to set
	 */
	public void setSkills(String[] skills)
	{
		this.skills = skills;
	}
	/**
	 * @return the jobSearchAddress
	 */
	public JobSearchAddress getJobSearchAddress()
	{
		return jobSearchAddress;
	}
	/**
	 * @param jobSearchAddress the jobSearchAddress to set
	 */
	public void setJobSearchAddress(JobSearchAddress jobSearchAddress)
	{
		this.jobSearchAddress = jobSearchAddress;
	}
	/**
	 * @return the transportation
	 */
	public String getTransportation()
	{
		return transportation;
	}
	/**
	 * @param transportation the transportation to set
	 */
	public void setTransportation(String transportation)
	{
		this.transportation = transportation;
	}
	/**
	 * @return the hasDriversLicense
	 */
	public Boolean getHasDriversLicense()
	{
		return hasDriversLicense;
	}
	/**
	 * @param hasDriversLicense the hasDriversLicense to set
	 */
	public void setHasDriversLicense(Boolean hasDriversLicense)
	{
		this.hasDriversLicense = hasDriversLicense;
	}
	/**
	 * @return the availability
	 */
	public Day[] getAvailability()
	{
		return availability;
	}
	/**
	 * @param availability the availability to set
	 */
	public void setAvailability(Day[] availability)
	{
		this.availability = availability;
	}
	/**
	 * @return the phone
	 */
	public String getPhone()
	{
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}
	/**
	 * @return the name
	 */
	public FullName getName()
	{
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(FullName name)
	{
		this.name = name;
	}
	/**
	 * @return the age
	 */
	public Integer getAge()
	{
		return age;
	}
	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age)
	{
		this.age = age;
	}
	/**
	 * @return the guid
	 */
	public String getGuid()
	{
		return guid;
	}
	/**
	 * @param guid the guid to set
	 */
	public void setGuid(String guid)
	{
		this.guid = guid;
	}
	/**
	 * @return the userId
	 */
	public Integer getUserId()
	{
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	
	
}
