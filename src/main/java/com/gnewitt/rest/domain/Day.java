package com.gnewitt.rest.domain;

public class Day
{
	String title;
	Integer dayIndex;
	
	/**
	 * 
	 */
	public Day()
	{
		super();
	}
	/**
	 * @param title
	 * @param dayIndex
	 */
	public Day(String title, Integer dayIndex)
	{
		super();
		this.title = title;
		this.dayIndex = dayIndex;
	}
	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}
	/**
	 * @return the dayIndex
	 */
	public Integer getDayIndex()
	{
		return dayIndex;
	}
	/**
	 * @param dayIndex the dayIndex to set
	 */
	public void setDayIndex(Integer dayIndex)
	{
		this.dayIndex = dayIndex;
	}
	
}
