package com.gnewitt.rest.domain;

public class Location
{
	Double longitude;
	Double latitude;
	/**
	 * @return the longitude
	 */
	public String getLongitude()
	{
		return longitude.toString();
	}
	/**
	 * @return the longitude
	 */
	public Double getLongitudeDouble()
	{
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) throws NumberFormatException
	{
			this.longitude = Double.valueOf(longitude);
	}
	/**
	 * @return the latitude
	 */
	public String getLatitude()
	{
		return latitude.toString();
	}
	/**
	 * @return the latitude
	 */
	public Double getLatitudeDouble()
	{
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) throws NumberFormatException
	{
			this.latitude = Double.valueOf(latitude);
	}
	
	
}
