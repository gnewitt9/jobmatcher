package com.gnewitt.rest.domain;

public class JobSearchAddress
{
	static final String MILES="mi";
	static final String NAUTICAL_MILES="nm";
	static final String KILOMETRES="km";
	String unit;
	Integer maxJobDistance;
	Double longitude;
	Double latitude;
	/**
	 * @return the unit
	 */
	public String getUnit()
	{
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit)
	{
		this.unit = unit;
	}
	/**
	 * @return the maxJobDistance
	 */
	public Integer getMaxJobDistance()
	{
		return maxJobDistance;
	}
	/**
	 * @param maxJobDistance the maxJobDistance to set
	 */
	public void setMaxJobDistance(Integer maxJobDistance)
	{
		this.maxJobDistance = maxJobDistance;
	}
	/**
	 * @return the longitude
	 */
	public String getLongitude()
	{
		return longitude.toString();
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) throws NumberFormatException
	{
			this.longitude = Double.valueOf(longitude);
	}
	/**
	 * @return the latitude
	 */
	public String getLatitude()
	{
		return latitude.toString();
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) throws NumberFormatException
	{
			this.latitude = Double.valueOf(latitude);
	}
	
    public int distance(Location location) 
	{
		return (distance(location,getUnit()));
	}


	public int distance(Location location, String unit) 
	{
		return (distance(location.getLatitudeDouble(),location.getLongitudeDouble(),unit).intValue());
	}

	public Double distance(double lat1, double lon1, String unit) {
		double theta = lon1 - longitude;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(latitude)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(latitude)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;
		if (KILOMETRES.equalsIgnoreCase(unit)) {
			dist = dist * 1.609344;
		} 
		else if (NAUTICAL_MILES.equalsIgnoreCase(unit)) 
		{
			dist = dist * 0.8684;
		}

		return (dist);
	}

	/*
		convert decimal degrees to radians						 
	*/
	private static double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/*
		This function converts radians to decimal degrees
	*/
	private static double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}

}
