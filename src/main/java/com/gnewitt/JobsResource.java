package com.gnewitt;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.gnewitt.rest.domain.Job;
import com.gnewitt.rest.domain.Worker;

/**
 * Root resource (exposed at "bestJobs" path)
 */
@Path("")
public class JobsResource 
{
	final static int NUMJOBS=3;
	final static String API="api",JOBS="jobs",WORKERS="workers";

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     * @throws IOException 
     * @throws MalformedURLException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     *
    */
	@Path(JOBS+"/{workerId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Job[] getJobs(@PathParam("workerId") String workerId) 
    {

        ClientConfig config = new ClientConfig().register(JacksonJsonProvider.class);

        Client client = ClientBuilder.newClient(config);

        WebTarget target = client.target(getBaseURI());
        
        
        System.out.println("Worker Id = '"+workerId+"'");
        Integer id=Integer.valueOf(workerId);

       	System.out.println("Id = "+id);
        Job jobs[]= 
                target.path(API).path(JOBS).request().accept(MediaType.APPLICATION_JSON).get(Job[].class);

        Worker workers[]= 
                target.path(API).path(WORKERS).request().accept(MediaType.APPLICATION_JSON).get(Worker[].class);
        
        Worker workerInQuestion=null;
        for (int i=0;i<workers.length;i++)
        {
    		workerInQuestion=workers[i];
        	if (workerInQuestion.getUserId()==id)
        	{
        		break;
        	}
        	workerInQuestion=null;
        }
        Job bestJobs[]=null;

        if (workerInQuestion!=null)
        {
        	bestJobs=getJobs(workerInQuestion,jobs);
        }
        
        return bestJobs;
      }

      private Job[] getJobs(Worker workerInQuestion, Job[] jobs)
	{
		int numJobs=NUMJOBS;
		if (jobs.length<=numJobs)
		{
			return jobs;
		}
		else
		{
			JobsSelector selector=new JobsSelector(workerInQuestion,jobs,NUMJOBS);
			List<Job> acceptedJobs=selector.selectJobs();
			return acceptedJobs.toArray(new Job[acceptedJobs.size()]);
		}
	}

	private static URI getBaseURI()
	{
        return UriBuilder.fromUri("http://swipejobs.azurewebsites.net/").build();
    }      
	
}
