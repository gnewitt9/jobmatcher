package com.gnewitt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.gnewitt.rest.domain.Day;
import com.gnewitt.rest.domain.Job;
import com.gnewitt.rest.domain.JobSearchAddress;
import com.gnewitt.rest.domain.Location;
import com.gnewitt.rest.domain.Worker;

public class JobsSelector
{
	final static int REJECT_THRESHOLD=80;
	final static int ACCEPT_THRESHOLD=20;
	private final int alpha[]={10,10,10,10,10,10,10};
	private final int beta[]={10,10,10,10,10,10,10};
	Worker worker;
	List<Job> jobs;
	Integer numJobs;

	public JobsSelector(Worker workerInQuestion, Job[] jobs, Integer numbJobs)
	{
		worker=workerInQuestion;
		this.jobs=Arrays.asList(jobs);
		this.numJobs=numbJobs;
	}
	
	public List<Job> selectJobs()
	{
		List<Job> nonRejectedJobs=rejectJobs(worker,jobs);
		List<Job> acceptableJobs=getAcceptableJobs(worker, nonRejectedJobs);
		return acceptableJobs;
	}
	
  	protected List<Job> rejectJobs(Worker workerInQuestion, List<Job> jobs)
  	{
  		ArrayList<ImmutablePair<Integer,Job>> rejectionScores=new ArrayList<ImmutablePair<Integer,Job>>(jobs.size());
  		int totalRejectionScores=0;
  		for (Job job:jobs)
  		{
  			int rejectionScore=0;
  			Job jobInQuestion=job;
  			if (!workerInQuestion.getIsActive())
  				rejectionScore+=alpha[2];
  			if ((jobInQuestion.getDriverLicenseRequired()) && (!workerInQuestion.getHasDriversLicense()))
  				rejectionScore+=alpha[2];
  			
  				rejectionScore+=alpha[3]*checkRequiredCertificates(workerInQuestion.getCertificates(),jobInQuestion.getRequiredCertificates());
  			
  			rejectionScore+=alpha[4]*checkNegativeLocation(workerInQuestion.getJobSearchAddress(),jobInQuestion.getLocation());
  			rejectionScore+=alpha[5]*checkStartDateVsAvailability(workerInQuestion.getAvailability(),jobInQuestion.getStartDate());
  			ImmutablePair<Integer,Job> pair= new ImmutablePair<Integer,Job>(rejectionScore,job);
  			rejectionScores.add(pair);
  			totalRejectionScores+=rejectionScore;
  		}
  		
		rejectionScores.sort(COMPARATOR);
  		int averageRejectionScore=totalRejectionScores/jobs.size();
  		int rejectionScore=averageRejectionScore-REJECT_THRESHOLD;
  		List<Job> nonRejectedJobs=new ArrayList<Job>(jobs.size());
  		int i=0;
  		for (Job job:jobs)
  		{
  			int rejectScore=rejectionScores.get(i).getLeft();
  			if (rejectScore<rejectionScore)
  				nonRejectedJobs.add(job);
  			i++;
  		}

  		return nonRejectedJobs;
  	}

  	protected List<Job> getAcceptableJobs(Worker workerInQuestion, List<Job> jobs)
	{
  		ArrayList<ImmutablePair<Integer,Job>> acceptanceScores=new ArrayList<ImmutablePair<Integer,Job>>(jobs.size());
		
  		int numResults=0;

  		for (Job job:jobs)
		{
			int acceptanceScore=0;
			Job jobInQuestion=job;
					
			acceptanceScore+=beta[0]*checkPositiveLocation(workerInQuestion.getJobSearchAddress(),jobInQuestion.getLocation());
			if (acceptanceScore>ACCEPT_THRESHOLD)
			{
	  			ImmutablePair<Integer,Job> pair= new ImmutablePair<Integer,Job>(acceptanceScore,job);
				acceptanceScores.add(pair);
				numResults++;
			}
		}
		acceptanceScores.sort(REVERSE_COMPARATOR);
		if (numResults>this.numJobs) numResults=this.numJobs;
		System.out.println("numResults - "+numResults);
  		ArrayList<Job> acceptableJobs=new ArrayList<Job>(numResults);
		
  		for (int j=0;((j<numResults)&&(acceptanceScores.get(j)!=null));j++)
  		{
  			System.out.println("Score - "+acceptanceScores.get(j).getLeft()+" JobId - "+acceptanceScores.get(j).getRight().getJobId());
				acceptableJobs.add(acceptanceScores.get(j).getRight());
  		}
		System.out.println("numResults - "+acceptableJobs.size());

  		return acceptableJobs;
	}

  	protected int checkStartDateVsAvailability(Day[] availability, Date startDate)
	{
		Calendar calendar=Calendar.getInstance();
		calendar.setTime(startDate);
		int dayOfWeek=calendar.get(Calendar.DAY_OF_WEEK)-1;
		if (dayOfWeek==0) dayOfWeek=7;
		
		for (int i=0;(i<availability.length)&&(availability[i]!=null);i++)
		{
			if (availability[i].getDayIndex()==dayOfWeek)
			{
				return 0;
			}
		}		
		return 1;
	}

  	protected int checkNegativeLocation(JobSearchAddress jobSearchAddress,
			Location location)
	{
		int distance=jobSearchAddress.distance(location);
		int difference=distance-jobSearchAddress.getMaxJobDistance();
		if (difference>0)
			return difference;
		return 0;
	}

  	protected int checkPositiveLocation(JobSearchAddress jobSearchAddress,
			Location location)
	{
		int distance=jobSearchAddress.distance(location);
		int difference=-(distance-jobSearchAddress.getMaxJobDistance());
		return difference;
	}

  	protected int checkRequiredCertificates(String[] certificates,
			String[] requiredCertificates)
	{
		int certsMissing=0;
		for (int i=0;i<requiredCertificates.length;i++)
		{
			String requiredCert=requiredCertificates[i];
			int missing=1;
			for (int j=0;j<certificates.length;j++)
			{
				if (requiredCert.equalsIgnoreCase(certificates[j]))
				{
					missing=0;
					break;
				}
			}
			certsMissing+=missing;
		}
		return certsMissing;
	}
  	public static final Comparator<Pair<Integer, Job>> COMPARATOR =
  			new Comparator<Pair<Integer,Job>>() {

  			public int compare(final Pair<Integer,Job> p1,
  			     final Pair<Integer,Job> p2) {

  			     int result = p1.getLeft().compareTo(p2.getLeft());
  			     return result;
  			}
  			};

  		  	public static final Comparator<Pair<Integer, Job>> REVERSE_COMPARATOR =
  		  			new Comparator<Pair<Integer,Job>>() {

  		  			public int compare(final Pair<Integer,Job> p1,
  		  			     final Pair<Integer,Job> p2) {

  		  			     int result = p2.getLeft().compareTo(p1.getLeft());
  		  			     return result;
  		  			}
  		  			};


}

