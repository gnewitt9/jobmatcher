/**
 * 
 */
package com.gnewitt;

import static org.junit.Assert.*;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import com.gnewitt.rest.domain.Job;

/**
 * @author Greg Newitt
 *
 */
public class JobsResourceTest extends JerseyTest
{

	
	@Override
	protected Application configure()
	{
		return new ResourceConfig(JobsResource.class);
	}

	/**
	 * Test method for {@link com.gnewitt.JobsResource#getJobs(java.lang.String)}.
	 */
	@Test
	public final void testGetJobs()
	{
        Job jobs[]=target("jobs/1").request().accept(MediaType.APPLICATION_JSON).get(Job[].class);
        assertEquals("",1,(int)jobs[0].getJobId());
        assertEquals("",11,(int)jobs[1].getJobId());
        assertEquals("",12,(int)jobs[2].getJobId());
  		for (Job job:jobs)
  		{
  			System.out.println("job - "+job.getJobId());
  		}
        jobs=target("jobs/2").request().accept(MediaType.APPLICATION_JSON).get(Job[].class);
  		for (Job job:jobs)
  		{
  			System.out.println("job - "+job.getJobId());
  		}
  		
        assertEquals("",1,(int)jobs[0].getJobId());
        jobs=target("jobs/3").request().accept(MediaType.APPLICATION_JSON).get(Job[].class);
  		for (Job job:jobs)
  		{
  			System.out.println("job - "+job.getJobId());
  		}
  		
        assertEquals("",8,(int)jobs[0].getJobId());
        assertEquals("",10,(int)jobs[1].getJobId());
  		
        jobs=target("jobs/4").request().accept(MediaType.APPLICATION_JSON).get(Job[].class);
  		for (Job job:jobs)
  		{
  			System.out.println("job - "+job.getJobId());
  		}
  		
        assertEquals("",8,(int)jobs[0].getJobId());
        assertEquals("",10,(int)jobs[1].getJobId());
  		
	}

}
