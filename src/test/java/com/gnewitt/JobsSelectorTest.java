/**
 * 
 */
package com.gnewitt;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.gnewitt.JobsSelector;
import com.gnewitt.rest.domain.Job;
import com.gnewitt.rest.domain.Worker;

/**
 * @author Greg Newitt
 *
 */
public class JobsSelectorTest
{
	JobsSelector js;
	Worker workerInQuestion, workers[];
	Job jobs[];
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception
	{
		jobs=DataFixture.populateAllJobs();
		workers=DataFixture.populateAllWorkers();
		workerInQuestion= DataFixture.populateWorker1();
		workerInQuestion = workers[1];
		js=new JobsSelector(workerInQuestion, jobs,3);
	}

	/**
	 * Test method for {@link com.gnewitt.JobsSelector#selectJobs()}.
	 */
	@Test
	public final void testSelectJobs()
	{
		List<Job> returnedJobs;
		returnedJobs=js.selectJobs();
		assertEquals("Wrong number of jobs in result.",3, returnedJobs.size());
	}
	
	@Test
	public final void testSelectJobsStartDayWrong()
	{
		workerInQuestion= DataFixture.populateWorker1();
		js=new JobsSelector(workerInQuestion, jobs,3);

		List<Job> returnedJobs;
		returnedJobs=js.selectJobs();
		assertEquals("Wrong number of jobs in result.",0,returnedJobs.size());
	}
}
