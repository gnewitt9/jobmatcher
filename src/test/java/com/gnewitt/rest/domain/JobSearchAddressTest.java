package com.gnewitt.rest.domain;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JobSearchAddressTest
{
	JobSearchAddress jsa;
	Location loc1;
	@Before
	public void setUp() throws Exception
	{
    	jsa=new JobSearchAddress();
    	jsa.setLatitude("29.46786");
    	jsa.setLongitude("-98.53506");
    	jsa.setUnit("km");
    	jsa.setMaxJobDistance(10);
    	loc1=new Location(); 
    	loc1.setLatitude("32.9697");
    	loc1.setLongitude("-96.80322");
		
	}

	@Test
	public final void testDistanceLocation()
	{
		int dist=jsa.distance(loc1);
		assertEquals("Distance incorrect",422,dist); 
	}

	@Test
	public final void testDistanceLocationString()
	{
		int dist=jsa.distance(loc1,"nm");
		assertEquals("Distance incorrect",228,dist); 
	}

	@Test
	public final void testDistanceDoubleDoubleString()
	{
		double delta=0.001,dist=jsa.distance(29.46786d, -98.53506d, JobSearchAddress.KILOMETRES).intValue();
		assertEquals("Distance incorrect",0,dist,delta); 
		dist=jsa.distance(32.9697d, -96.80322d, JobSearchAddress.KILOMETRES);
		assertEquals("Distance incorrect",422.7389,dist,delta); 
		dist=jsa.distance(32.9697d, -96.80322d, JobSearchAddress.NAUTICAL_MILES);
		assertEquals("Distance incorrect",228.1093,dist,delta); 
		dist=jsa.distance(32.9697d, -96.80322d, JobSearchAddress.MILES);
		assertEquals("Distance incorrect",262.6777,dist,delta); 

	}


}
