package com.gnewitt;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.gnewitt.rest.domain.Day;
import com.gnewitt.rest.domain.FullName;
import com.gnewitt.rest.domain.Job;
import com.gnewitt.rest.domain.JobSearchAddress;
import com.gnewitt.rest.domain.Location;
import com.gnewitt.rest.domain.Worker;

public class DataFixture
{
	static final Day monday=new Day("Monday",1);
	static final Day tuesday=new Day("Tuesday",2);
	static final Day wednesday=new Day("Wednesday",3);
	static final Day thursday=new Day("Thursday",4);
	static final Day friday=new Day("Friday",5);
	static final Day saturday=new Day("Saturday",6);
	static final Day sunday=new Day("Sunday",7);
	
	public static JobSearchAddress populateJSA(String latitude, String longitude, Integer maxJobDistance, String unit)
	{
		JobSearchAddress jobSearchAddress=new JobSearchAddress();
		jobSearchAddress.setLatitude(latitude);
		jobSearchAddress.setLongitude(longitude);
		jobSearchAddress.setMaxJobDistance(maxJobDistance);
		jobSearchAddress.setUnit(unit);
		return jobSearchAddress;
	}
	
	public static JobSearchAddress populateJSA1()
	{
		return populateJSA("49.782281","49.782281",30,"km");
	}
	
	public static Worker[] populateAllWorkers()
	{
        ClientConfig config = new ClientConfig().register(JacksonJsonProvider.class);

        Client client = ClientBuilder.newClient(config);

        WebTarget target = client.target(getBaseURI());
        
        Worker workers[]= 
                target.path("api").path("workers").request().accept(MediaType.APPLICATION_JSON).get(Worker[].class);
        
		return workers;
	}
	
	
	public static Worker populateWorker(Integer rating, Boolean active, JobSearchAddress jsa, String[] certs, Day[] avail, String[] skills, String transport, Boolean license, String phone, String email, FullName name, String guid, Integer uid, Integer age)
	{
		Worker workerInQuestion=new Worker();
		workerInQuestion.setRating(rating);
		workerInQuestion.setIsActive(active);
		workerInQuestion.setJobSearchAddress(jsa);
		workerInQuestion.setCertificates(certs);
		workerInQuestion.setAvailability(avail);
		workerInQuestion.setSkills(skills);
		workerInQuestion.setTransportation(transport);
		workerInQuestion.setHasDriversLicense(license);
		workerInQuestion.setPhone(phone);
		workerInQuestion.setEmail(email);
		workerInQuestion.setName(name);
		workerInQuestion.setGuid(guid);
		workerInQuestion.setUserId(uid);
		workerInQuestion.setAge(age);
		return workerInQuestion;
	}
	
	public static Worker populateWorker1()
	{
		Worker workerInQuestion=new Worker();
		workerInQuestion.setRating(2);
		workerInQuestion.setIsActive(true);
		workerInQuestion.setJobSearchAddress(populateJSA1());
		workerInQuestion.setCertificates(populateCertificates1());
		workerInQuestion.setAvailability(populateNoAvailability());
		workerInQuestion.setSkills(populateSkills1());
		workerInQuestion.setTransportation("CAR");
		workerInQuestion.setHasDriversLicense(false);
		workerInQuestion.setPhone("+1 (847) 420-3272");
		workerInQuestion.setEmail("fowler.andrews@comcubine.io");
		workerInQuestion.setName(populateName1());
		workerInQuestion.setGuid("562f6647410ecd6bf49146e9");
		workerInQuestion.setUserId(0);
		workerInQuestion.setAge(30);
		return populateWorker(2,true,populateJSA1(),populateCertificates1(),populateAvailabilitySundayToMonday(),populateSkills1(),"CAR",
				false,"+1 (847) 420-3272","fowler.andrews@comcubine.io",populateName1(),"562f6647410ecd6bf49146e9",0,30);
	}
	
	public static Day[] populateAvailablity(Day[] days)
	{
		return days;
	}
	
	public static Day[] populateAvailabilitySundayToMonday()
	{
		Day[] availability={monday,tuesday,wednesday,thursday,friday,sunday};
		return populateAvailablity(availability);
	}
	
	public static Day[] populateNoAvailability()
	{
		Day[] availability={};
		return populateAvailablity(availability);
	}
	
	public static String[] populateCertificates(String certificates[])
	{
		return certificates;
	}
	
	public static String[] populateCertificates1()
	{
		String certificates[]={"Outstanding Innovator","The Behind the Scenes Wonder","The Risk Taker","Outside the Box Thinker",
		    "Marvelous Multitasker","The Asker of Good Questions","Outstanding Memory Award","Office Lunch Expert","Excellence in Organization"};
		return populateCertificates(certificates);
	}
     
	public static String[] populateCertificates2()
	{
		String certificates[]={	    "Excellence in Organization","Healthy Living Promoter"};
		return populateCertificates(certificates);
	}
     
	public static String[] populateSkills(String skills[])
	{
		return skills;
	}
	
	public static String[] populateSkills1()
	{
	    String[] skills={"Creator of opportunities","Arts and Crafts Designer"};
		return populateSkills(skills);
	}
	
	public static FullName populateName(String first,String last)
	{
		FullName name=new FullName();
		name.setFirst(first);
		name.setLast(last);
		return name;
	}
	
	public static FullName populateName1()
	{
		return populateName("Fowler", "Andrews");
	}

	public static Location populateLocation(String latitude,String longitude)
	{
		Location location=new Location();
		location.setLatitude(latitude);
		location.setLongitude(longitude);
		return location;
	}
	
	public static Location populateLocation1()
	{
		return populateLocation("13.864602","49.93359");
	}
	public static Location populateLocation2()
	{
		return populateLocation("14.220885","50.176094");
	}
	
	public static Job[] populateJobs()
	{
		Job jobs[]={populateJob1(),populateJob2()};
		return jobs;
	}

	public static Job[] populateAllJobs()
	{
        ClientConfig config = new ClientConfig().register(JacksonJsonProvider.class);

        Client client = ClientBuilder.newClient(config);

        WebTarget target = client.target(getBaseURI());
        
        
        Job jobs[]= 
                target.path("api").path("jobs").request().accept(MediaType.APPLICATION_JSON).get(Job[].class);
        
		return jobs;
	}
	
	private static URI getBaseURI() {
        return UriBuilder.fromUri("http://swipejobs.azurewebsites.net/").build();
      }

	public static Job populateJob(Boolean driverLicenseRequired, String[] requiredCertificates, Location location, String billRate,Integer workersRequired,String startTime, String about, String jobTitle, String company, String guid, Integer jobId)
	{
		Job job=new Job();
		job.setDriverLicenseRequired(driverLicenseRequired);
		job.setRequiredCertificates(requiredCertificates);
		job.setLocation(location);
		job.setBillRate(billRate);
		job.setWorkersRequired(workersRequired);
		SimpleDateFormat parserSDF=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		Date startDate;
		try
		{
			startDate = parserSDF.parse(startTime);
		}
		catch (ParseException e)
		{
			startDate=new Date();
		}
		job.setStartDate(startDate);
		job.setAbout(about);
		job.setJobTitle(jobTitle);
		job.setCompany(company);
		job.setGuid(guid);
		job.setJobId(jobId);
		return job;
	}
	
	public static Job populateJob1()
	{
		String about="Enim excepteur ex dolore commodo incididunt. Mollit officia laborum sunt nostrud id duis non. Minim consectetur enim in dolore ipsum nulla. Occaecat irure voluptate esse ut do est nostrud esse fugiat.";
		return populateJob(false, populateCertificates1(),populateLocation1(),"$6.16",1,"2015-11-12T09:29:19.188",about,"The Resinator","Frenex","562f66aa51a9a4d728a65f6a",0);
	}
	public static Job populateJob2()
	{
		String about="Ad nostrud eiusmod ullamco ut eiusmod excepteur non. Veniam aliquip sit ullamco dolor consectetur esse sit fugiat incididunt reprehenderit. Eiusmod anim duis eu irure est sit dolor esse labore fugiat qui proident tempor duis. Incididunt Lorem non sit reprehenderit eu labore veniam. Id velit ullamco in ut pariatur consequat ut occaecat. Laboris adipisicing incididunt quis sint irure cillum velit ad culpa quis cupidatat officia est.";
		return populateJob(true, populateCertificates2(),populateLocation2(),"$6.68",3,"2015-11-08T02:25:45.091",about,"President and TeaEO","Orbiflex","562f66aa17b61ab099669893",1);
	}
}
